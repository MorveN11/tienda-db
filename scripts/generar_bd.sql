DROP DATABASE IF EXISTS supermercado_bd;

CREATE DATABASE IF NOT EXISTS supermercado_bd DEFAULT CHARACTER
SET
    utf8 COLLATE utf8_general_ci;

USE supermercado_bd;

CREATE TABLE IF NOT EXISTS categoria(
    id VARCHAR(36) NOT NULL,
    nombre VARCHAR(255) UNIQUE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS preferencia(
    id VARCHAR(36) NOT NULL,
    tipo VARCHAR(255) UNIQUE NOT NULL,
    categoria_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (categoria_id) REFERENCES categoria(id)
);

CREATE TABLE IF NOT EXISTS sector(
    id VARCHAR(36) NOT NULL,
    identificador VARCHAR(255) UNIQUE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS estante(
    id VARCHAR(36) NOT NULL,
    ubicacion VARCHAR(255) UNIQUE NOT NULL,
    categoria_id VARCHAR(36) NOT NULL,
    sector_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (categoria_id) REFERENCES categoria(id),
    FOREIGN KEY (sector_id) REFERENCES sector(id)
);

CREATE TABLE IF NOT EXISTS cliente(
    id VARCHAR(36) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    zona VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS telefono(
    id VARCHAR(36) NOT NULL,
    numero VARCHAR(255) NOT NULL,
    cliente_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (cliente_id) REFERENCES cliente(id)
);

CREATE TABLE IF NOT EXISTS cliente_preferencias(
    cliente_id VARCHAR(36) NOT NULL,
    preferencia_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (cliente_id, preferencia_id),
    FOREIGN KEY (cliente_id) REFERENCES cliente(id),
    FOREIGN KEY (preferencia_id) REFERENCES preferencia(id)
);

CREATE TABLE IF NOT EXISTS caja(
    id VARCHAR(36) NOT NULL,
    identificador VARCHAR(255) UNIQUE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS historial(
    id VARCHAR(36) NOT NULL,
    fecha DATE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS historial_cajas(
    historial_id VARCHAR(36) NOT NULL,
    caja_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (historial_id, caja_id),
    FOREIGN KEY (historial_id) REFERENCES historial(id),
    FOREIGN KEY (caja_id) REFERENCES caja(id)
);

CREATE TABLE IF NOT EXISTS turno(
    id VARCHAR(36) NOT NULL,
    hora_inicio TIME NOT NULL,
    hora_fin TIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cargo(
    id VARCHAR(36) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS empleado(
    id VARCHAR(36) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    telefono VARCHAR(255) NOT NULL,
    cargo_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (cargo_id) REFERENCES cargo(id)
);

CREATE TABLE IF NOT EXISTS empleado_salario(
    id VARCHAR(36) NOT NULL,
    sueldo_base DECIMAL(10, 2) NOT NULL,
    empleado_id VARCHAR(36) UNIQUE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (empleado_id) REFERENCES empleado(id)
);

CREATE TABLE IF NOT EXISTS historial_pago(
    id VARCHAR(36) NOT NULL,
    fecha_emision DATE NOT NULL,
    fecha_pago DATE NOT NULL,
    mes_pago VARCHAR(255) NOT NULL,
    empleado_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (empleado_id) REFERENCES empleado(id)
);

CREATE TABLE IF NOT EXISTS bono(
    id VARCHAR(36) NOT NULL,
    monto DECIMAL(10, 2) NOT NULL,
    razon VARCHAR(255) NOT NULL,
    historial_pago_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (historial_pago_id) REFERENCES historial_pago(id)
);

CREATE TABLE IF NOT EXISTS descuento(
    id VARCHAR(36) NOT NULL,
    monto DECIMAL(10, 2) NOT NULL,
    razon VARCHAR(255) NOT NULL,
    historial_pago_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (historial_pago_id) REFERENCES historial_pago(id)
);

CREATE TABLE IF NOT EXISTS historial_turnos(
    historial_id VARCHAR(36) NOT NULL,
    turno_id VARCHAR(36) NOT NULL,
    empleado_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (historial_id, turno_id),
    FOREIGN KEY (historial_id) REFERENCES historial(id),
    FOREIGN KEY (turno_id) REFERENCES turno(id),
    FOREIGN KEY (empleado_id) REFERENCES empleado(id)
);

CREATE TABLE IF NOT EXISTS venta(
    id VARCHAR(36) NOT NULL,
    fecha DATE NOT NULL,
    hora TIME NOT NULL,
    caja_id VARCHAR(36) NOT NULL,
    cliente_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (caja_id) REFERENCES caja(id),
    FOREIGN KEY (cliente_id) REFERENCES cliente(id)
);

CREATE TABLE IF NOT EXISTS venta_total(
    id VARCHAR(36) NOT NULL,
    total DECIMAL(10, 2) NOT NULL,
    venta_id VARCHAR(36) UNIQUE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (venta_id) REFERENCES venta(id)
);

CREATE TABLE IF NOT EXISTS procedencia(
    id VARCHAR(36) NOT NULL,
    pais VARCHAR(255) UNIQUE NOT NULL,
    ciudad VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS lote(
    id VARCHAR(36) NOT NULL,
    identificador VARCHAR(255) UNIQUE NOT NULL,
    procedencia_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (procedencia_id) REFERENCES procedencia(id)
);

CREATE TABLE IF NOT EXISTS marca(
    id VARCHAR(36) NOT NULL,
    nombre VARCHAR(255) UNIQUE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS producto(
    id VARCHAR(36) NOT NULL,
    codigo VARCHAR(255) UNIQUE NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    peso DECIMAL(10, 2) NOT NULL,
    categoria_id VARCHAR(36) NOT NULL,
    lote_id VARCHAR(36) NOT NULL,
    marca_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (categoria_id) REFERENCES categoria(id),
    FOREIGN KEY (lote_id) REFERENCES lote(id),
    FOREIGN KEY (marca_id) REFERENCES marca(id)
);

CREATE TABLE IF NOT EXISTS producto_precio(
    id VARCHAR(36) NOT NULL,
    precio_compra DECIMAL(10, 2) NOT NULL,
    precio_venta DECIMAL(10, 2) NOT NULL,
    producto_id VARCHAR(36) UNIQUE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (producto_id) REFERENCES producto(id)
);

CREATE TABLE IF NOT EXISTS color(
    id VARCHAR(36) NOT NULL,
    nombre VARCHAR(255) UNIQUE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS producto_colores(
    producto_id VARCHAR(36) NOT NULL,
    color_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (producto_id, color_id),
    FOREIGN KEY (producto_id) REFERENCES producto(id),
    FOREIGN KEY (color_id) REFERENCES color(id)
);

CREATE TABLE IF NOT EXISTS promocion(
    id VARCHAR(36) NOT NULL,
    nombre VARCHAR(255) UNIQUE NOT NULL,
    precio DECIMAL(10, 2) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS promocion_calificacion(
    id VARCHAR(36) NOT NULL,
    calificacion INT NOT NULL,
    promocion_id VARCHAR(36) UNIQUE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (promocion_id) REFERENCES promocion(id)
);

CREATE TABLE IF NOT EXISTS promocion_duracion(
    id VARCHAR(36) NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    promocion_id VARCHAR(36) UNIQUE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (promocion_id) REFERENCES promocion(id)
);

CREATE TABLE IF NOT EXISTS promocion_productos(
    promocion_id VARCHAR(36) NOT NULL,
    producto_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (promocion_id, producto_id),
    FOREIGN KEY (promocion_id) REFERENCES promocion(id),
    FOREIGN KEY (producto_id) REFERENCES producto(id)
);

CREATE TABLE IF NOT EXISTS venta_productos(
    venta_id VARCHAR(36) NOT NULL,
    producto_id VARCHAR(36) NOT NULL,
    cantidad INT NOT NULL,
    PRIMARY KEY (venta_id, producto_id),
    FOREIGN KEY (venta_id) REFERENCES venta(id),
    FOREIGN KEY (producto_id) REFERENCES producto(id)
);

CREATE TABLE IF NOT EXISTS venta_promociones(
    venta_id VARCHAR(36) NOT NULL,
    promocion_id VARCHAR(36) NOT NULL,
    cantidad INT NOT NULL,
    PRIMARY KEY (venta_id, promocion_id),
    FOREIGN KEY (venta_id) REFERENCES venta(id),
    FOREIGN KEY (promocion_id) REFERENCES promocion(id)
);
