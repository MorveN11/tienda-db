-- inciso a
SELECT
    c.nombre AS cliente_nombre,
    c.apellido AS cliente_apellido,
    GROUP_CONCAT(t.numero) AS cliente_telefonos,
    v.fecha AS fecha_venta,
    v.hora AS hora_venta,
    vt.total AS monto_venta
FROM
    cliente c
    LEFT JOIN telefono t ON c.id = t.cliente_id
    INNER JOIN venta v ON c.id = v.cliente_id
    INNER JOIN venta_total vt ON v.id = vt.venta_id
    INNER JOIN venta_productos vp ON v.id = vp.venta_id
    INNER JOIN producto p ON vp.producto_id = p.id
    INNER JOIN categoria cat ON p.categoria_id = cat.id
    INNER JOIN caja ca ON v.caja_id = ca.id
    INNER JOIN historial_cajas hc ON ca.id = hc.caja_id
    INNER JOIN historial h ON hc.historial_id = h.id
    INNER JOIN historial_turnos ht ON h.id = ht.historial_id
    INNER JOIN turno tu ON ht.turno_id = tu.id
WHERE
    (
        cat.nombre = 'Cristalería'
        OR cat.nombre = 'Juguetería'
    )
    AND tu.hora_inicio = '08:00:00'
    AND tu.hora_fin = '12:00:00'
    AND MONTH(v.fecha) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)
    AND YEAR(v.fecha) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
GROUP BY
    c.nombre,
    c.apellido,
    v.fecha,
    v.hora,
    vt.total
ORDER BY
    v.fecha,
    v.hora;

-- inciso b
SELECT
    p.nombre AS nombre_promocion,
    GROUP_CONCAT(pr.nombre SEPARATOR ', ') AS productos,
    pd.fecha_inicio,
    pd.fecha_fin,
    p.precio AS precio_promocion
FROM
    promocion p
    INNER JOIN promocion_productos pp ON p.id = pp.promocion_id
    INNER JOIN producto pr ON pp.producto_id = pr.id
    INNER JOIN promocion_duracion pd ON p.id = pd.promocion_id
    INNER JOIN promocion_calificacion pc ON p.id = pc.promocion_id
WHERE
    pc.calificacion >= 5 -- considerando que la calificación de 5 o mayor que es un exito
    AND MONTH(pd.fecha_inicio) BETWEEN 1 AND 9
    AND YEAR(pd.fecha_inicio) = YEAR(CURRENT_DATE)
GROUP BY
    p.nombre,
    pd.fecha_inicio,
    pd.fecha_fin,
    p.precio
ORDER BY
    pd.fecha_inicio;

-- inciso c
SELECT
    pr.nombre AS nombre_producto,
    pp.precio_venta AS costo_producto
FROM
    venta v
    INNER JOIN venta_productos vp ON v.id = vp.venta_id
    INNER JOIN producto pr ON vp.producto_id = pr.id
    INNER JOIN producto_precio pp ON pr.id = pp.producto_id
WHERE
    MONTH(v.fecha) BETWEEN 1 AND 3
    AND YEAR(v.fecha) = YEAR(CURRENT_DATE)
    AND pr.categoria_id = (
        SELECT
            id
        FROM
            categoria
        WHERE
            nombre = 'Ferretería'
    )
    AND pp.precio_venta BETWEEN 50 AND 250
GROUP BY
    pr.nombre,
    pp.precio_venta;

-- inciso d
SELECT
    pr.id AS id_producto,
    pr.nombre AS nombre_producto,
    pr.codigo AS codigo_producto,
    pr.peso AS peso_producto,
    ct.nombre AS categoria_producto,
    lt.identificador AS lote_producto,
    mr.nombre AS marca_producto
FROM
    producto pr
    INNER JOIN lote lt ON pr.lote_id = lt.id
    INNER JOIN marca mr ON pr.marca_id = mr.id
    INNER JOIN categoria ct ON pr.categoria_id = ct.id
WHERE
    pr.id NOT IN (
        SELECT
            DISTINCT vp.producto_id
        FROM
            venta_productos vp
    );
