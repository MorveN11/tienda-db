USE supermercado_bd;

INSERT INTO
    sector (id, identificador)
VALUES
    ('d0e4e7c0-26a7-4b5d-bb7e-82a15491b25c', 'A1'),
    ('a02a9eb3-8aae-41b8-8d3f-04bc6b6c5945', 'B2'),
    ('4a352b6f-5a9e-495c-b6f4-67c89b9a48fe', 'C3'),
    ('f3fdd8cf-04ab-4e94-ba9f-5e3e34f0c4b5', 'D4'),
    ('09a7eb43-48a3-4cfa-89e8-70bea5b3942a', 'E5'),
    ('4f5b2dd7-59d7-4a9b-8f1e-09d8e99d3780', 'F6'),
    ('8b4f1b8e-eb8e-4855-9c66-6c2732e74b5a', 'G7'),
    ('4482c20b-bd0b-4b19-ae5b-4bf66f9e7394', 'H8'),
    ('0f683005-d43b-4e9a-85d6-4c82ce024ef1', 'I9'),
    ('c3dd9d3a-2e7b-465a-a64f-1ed2b08020e8', 'J10');

INSERT INTO
    categoria (id, nombre)
VALUES
    (
        'ed3c5b8d-6b6f-4b7e-91f6-1e7c6bc9a2f0',
        'Cristalería'
    ),
    (
        '523ea6b4-6f32-49d6-b431-251cf30a78f8',
        'Electrodomésticos'
    ),
    (
        '6d1a2b8f-1f93-41cc-b208-45e7b1f23dab',
        'Comunicación'
    ),
    (
        'e70b0f7c-3bfe-4b18-9e7b-4efc0a1e0e4b',
        'Muebles'
    ),
    (
        'bbd8d9b8-5254-4a26-8a14-3bcb84b57212',
        'Ferretería'
    ),
    (
        '9d4a1067-99e1-45b4-8e58-dc8c0a53e39e',
        'Juguetería'
    ),
    (
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0',
        'Artículos en general'
    );

INSERT INTO
    preferencia (id, tipo, categoria_id)
VALUES
    (
        '1b69c922-56a0-4b61-91f4-d62b13d7a4c1',
        'Copas de Cristal',
        'ed3c5b8d-6b6f-4b7e-91f6-1e7c6bc9a2f0'
    ),
    (
        '2b7fc8ef-8493-4c02-a1d7-462e3c16f0a1',
        'Jarrones',
        'ed3c5b8d-6b6f-4b7e-91f6-1e7c6bc9a2f0'
    ),
    (
        '3c8f9a2d-9b28-4e1d-9c6f-3d2a6b9f8c7e',
        'Refrigerador',
        '523ea6b4-6f32-49d6-b431-251cf30a78f8'
    ),
    (
        '4d7e8b3c-5f19-4a2c-8d7b-1e4f5a6b7c8d',
        'Lavadora',
        '523ea6b4-6f32-49d6-b431-251cf30a78f8'
    ),
    (
        '5e6f7d4b-6c5a-4b3c-8a9d-2e3f4a5b6c7d',
        'Teléfono Móvil',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0'
    ),
    (
        '6f7e8d5c-7a6b-5c4d-9b8e-3c4d5e6f7a8b',
        'Tableta',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0'
    ),
    (
        '7g8h9i6j-8h7g-6i5j-0k9l-1m0n2o3p4q5',
        'Mesa de Centro',
        'e70b0f7c-3bfe-4b18-9e7b-4efc0a1e0e4b'
    ),
    (
        '8h9i0j7k-9l8m-0n9o-2p1q-3r4s5t6u7v8',
        'Sillón Reclinable',
        'e70b0f7c-3bfe-4b18-9e7b-4efc0a1e0e4b'
    ),
    (
        '9i0j1k8l-0m9n-1o0p-3q2r-4s5t6u7v8w9',
        'Taladro Eléctrico',
        'bbd8d9b8-5254-4a26-8a14-3bcb84b57212'
    ),
    (
        '0j1k2l9m-1n0o-2p1q-4r3s-5t6u7v8w9x0',
        'Destornillador',
        'bbd8d9b8-5254-4a26-8a14-3bcb84b57212'
    ),
    (
        '1k2l3m0n-2o1p-3q2r-5s4t-6u7v8w9x0y1',
        'Muñeca',
        '9d4a1067-99e1-45b4-8e58-dc8c0a53e39e'
    ),
    (
        '2l3m4n1o-3p2q-4r3s-6t5u-7v8w9x0y1z2',
        'Carro a Control Remoto',
        '9d4a1067-99e1-45b4-8e58-dc8c0a53e39e'
    ),
    (
        '3m4n5o2p-4q3r-5s4t-7u6v-8w9x0y1z2a3',
        'Artículos en General',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0'
    ),
    (
        '4n5o6p3q-5r4s-6t5u-8v7w-9x0y1z2a3b4',
        'Artículos Variados',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0'
    );

INSERT INTO
    estante (id, ubicacion, categoria_id, sector_id)
VALUES
    (
        '1a2b3c4d-5e6f-7a8b-9c0d-1e2f3a4b5c6d',
        'ES-A1-001',
        'ed3c5b8d-6b6f-4b7e-91f6-1e7c6bc9a2f0',
        'd0e4e7c0-26a7-4b5d-bb7e-82a15491b25c'
    ),
    (
        '2b3c4d5e-6f7a-8b9c-0d1e-2f3a4b5c6d7e',
        'ES-B2-002',
        'ed3c5b8d-6b6f-4b7e-91f6-1e7c6bc9a2f0',
        'a02a9eb3-8aae-41b8-8d3f-04bc6b6c5945'
    ),
    (
        '3c4d5e6f-7a8b-9c0d-1e2f-3a4b5c6d7e8f',
        'ES-C3-003',
        '523ea6b4-6f32-49d6-b431-251cf30a78f8',
        '4a352b6f-5a9e-495c-b6f4-67c89b9a48fe'
    ),
    (
        '4d5e6f7a-8b9c-0d1e-2f3a-4b5c6d7e8f9',
        'ES-D4-004',
        '523ea6b4-6f32-49d6-b431-251cf30a78f8',
        'f3fdd8cf-04ab-4e94-ba9f-5e3e34f0c4b5'
    ),
    (
        '5e6f7a8b-9c0d-1e2f-3a4b-5c6d7e8f9a0b',
        'ES-E5-005',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0',
        '09a7eb43-48a3-4cfa-89e8-70bea5b3942a'
    ),
    (
        '6f7a8b9c-0d1e-2f3a-4b5c-6d7e8f9a0b1c',
        'ES-F6-006',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0',
        '4f5b2dd7-59d7-4a9b-8f1e-09d8e99d3780'
    ),
    (
        '7a8b9c0d-1e2f-3a4b-5c6d-7e8f9a0b1c2d',
        'ES-G7-007',
        'e70b0f7c-3bfe-4b18-9e7b-4efc0a1e0e4b',
        '8b4f1b8e-eb8e-4855-9c66-6c2732e74b5a'
    ),
    (
        '8b9c0d1e-2f3a-4b5c-6d7e-8f9a0b1c2d3e',
        'ES-H8-008',
        'e70b0f7c-3bfe-4b18-9e7b-4efc0a1e0e4b',
        '4482c20b-bd0b-4b19-ae5b-4bf66f9e7394'
    ),
    (
        '9c0d1e2f-3a4b-5c6d-7e8f-9a0b1c2d3e4f',
        'ES-I9-009',
        'bbd8d9b8-5254-4a26-8a14-3bcb84b57212',
        '0f683005-d43b-4e9a-85d6-4c82ce024ef1'
    ),
    (
        '0d1e2f3a-4b5c-6d7e-8f9a-0b1c2d3e4f5a',
        'ES-J10-010',
        'bbd8d9b8-5254-4a26-8a14-3bcb84b57212',
        'c3dd9d3a-2e7b-465a-a64f-1ed2b08020e8'
    ),
    (
        '1e2f3a4b-5c6d-7e8f-9a0b-1c2d3e4f5a6b',
        'ES-K10-011',
        '9d4a1067-99e1-45b4-8e58-dc8c0a53e39e',
        '8b4f1b8e-eb8e-4855-9c66-6c2732e74b5a'
    ),
    (
        '2f3a4b5c-6d7e-8f9a-0b1c-2d3e4f5a6b7',
        'ES-J10-012',
        '9d4a1067-99e1-45b4-8e58-dc8c0a53e39e',
        '4482c20b-bd0b-4b19-ae5b-4bf66f9e7394'
    ),
    (
        '3a4b5c6d-7e8f-9a0b-1c2d-3e4f5a6b7c',
        'ES-I9-013',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0',
        '0f683005-d43b-4e9a-85d6-4c82ce024ef1'
    ),
    (
        '4b5c6d7e-8f9a-0b1c-2d3e-4f5a6b7c8d',
        'ES-H8-014',
        'e2bf1c3e-7418-4c89-a10f-036b2e08a5e0',
        'c3dd9d3a-2e7b-465a-a64f-1ed2b08020e8'
    );

INSERT INTO
    color (id, nombre)
VALUES
    ('b392c99f-2e47-4926-a6e3-6927d1c3b9d5', 'Rojo'),
    ('6d9a2e7c-11d7-4a5d-9a22-1191b6b1b2b8', 'Azul'),
    ('a5e7f8b4-6c1f-4fd2-9a3b-6f2d8b1a7c3e', 'Verde'),
    (
        '8f1b2e9c-4d6a-4982-ba4e-9c3a2e1f4b5d',
        'Amarillo'
    ),
    ('3c9a5e1b-8d6f-4a7b-9e2c-1f4d2e3a5b6c', 'Blanco'),
    ('7b8c9d5a-3e2f-4b6d-8c9a-5a3b2c1d8e9f', 'Negro'),
    (
        '1e2f3a4b-9c8d-4e5f-1a2b-3c4d5e6f7a8b',
        'Naranja'
    ),
    ('9c8a7b6d-2f1e-4d5c-9a8b-7c6d5e4f3a2b', 'Morado'),
    ('4e5f6a7b-1c9d-4b8a-6d5e-2f3a1b9c8d7e', 'Gris'),
    ('2d3e4f5a-6b5c-4a3b-3c2d-1e5f6a7b8c9d', 'Rosado');

INSERT INTO
    procedencia (id, pais, ciudad)
VALUES
    (
        '8d0e19c6-8d88-4c3f-89c8-8a5c2b51267c',
        'Bolivia',
        'La Paz'
    ),
    (
        'e2a0c2f4-2a8b-4e1e-9c5b-c4f5e6a7c2e0',
        'Peru',
        'Lima'
    ),
    (
        'f5b8a2c1-5e7f-4a6b-8d3c-9e0f1a2b3d4e',
        'Ecuador',
        'Quito'
    ),
    (
        '1c9a8b7f-3a4e-1b2c-9d5e-6f7a8b9c0d1e',
        'Chile',
        'Santiago'
    ),
    (
        '3d2e1f4a-6b5c-7d8e-9f0a-1b2c3d4e5f6a',
        'Argentina',
        'Buenos Aires'
    ),
    (
        '5b4c3d2e-7f6a-8b9c-0d1e-2f3a4b5c6d7e',
        'Brazil',
        'Sao Paulo'
    ),
    (
        '7e8f9a0b-1c2d-3e4f-5a6b-7c8d9e0f1a2b',
        'Colombia',
        'Bogota'
    ),
    (
        '9a1b2c3d-4e5f-6a7b-8c9d-0e1f2a3b4c5d',
        'Venezuela',
        'Caracas'
    ),
    (
        'b2c3d4e5-6f7a-8b9c-0d1e-2f3a4b5c6d7e',
        'Uruguay',
        'Montevideo'
    ),
    (
        'd4e5f6a7-8b9c-0d1e-2f3a-4b5c6d7e8f9a',
        'Paraguay',
        'Asuncion'
    );

INSERT INTO
    lote (id, identificador, procedencia_id)
VALUES
    (
        'd7faa42e-1f11-4c94-a05c-5f3f1e92b33f',
        'L-001',
        '8d0e19c6-8d88-4c3f-89c8-8a5c2b51267c'
    ),
    (
        '6a36b6f8-6a8a-4cc1-82e1-2d8134620e91',
        'L-002',
        'e2a0c2f4-2a8b-4e1e-9c5b-c4f5e6a7c2e0'
    ),
    (
        '2fa9f37b-4f5d-469f-8a5d-4e6a2b42e74a',
        'L-003',
        'f5b8a2c1-5e7f-4a6b-8d3c-9e0f1a2b3d4e'
    ),
    (
        '109e9aa4-b10a-4b69-9f08-68f188eb3770',
        'L-004',
        '1c9a8b7f-3a4e-1b2c-9d5e-6f7a8b9c0d1e'
    ),
    (
        'ee240317-14c5-4b20-b1b2-84ecf2c7c186',
        'L-005',
        '3d2e1f4a-6b5c-7d8e-9f0a-1b2c3d4e5f6a'
    ),
    (
        'ef17840a-bb6e-43d6-bd1c-b1c1c1db0d5b',
        'L-006',
        '5b4c3d2e-7f6a-8b9c-0d1e-2f3a4b5c6d7e'
    ),
    (
        '8f9a3c3a-6a4c-46e8-93f8-7d46f6c4f4e4',
        'L-007',
        '7e8f9a0b-1c2d-3e4f-5a6b-7c8d9e0f1a2b'
    ),
    (
        '8f0d3005-3ff2-4643-8f1c-bce2f4e160a7',
        'L-008',
        '9a1b2c3d-4e5f-6a7b-8c9d-0e1f2a3b4c5d'
    ),
    (
        'dd25b6d4-2158-4677-950b-3275a1c154db',
        'L-009',
        'b2c3d4e5-6f7a-8b9c-0d1e-2f3a4b5c6d7e'
    ),
    (
        '8aefc84f-6b52-49c1-a231-8d50f3cf3f2f',
        'L-010',
        'd4e5f6a7-8b9c-0d1e-2f3a-4b5c6d7e8f9a'
    );

INSERT INTO
    marca (id, nombre)
VALUES
    (
        'a31d9d5e-783e-4c2d-b85d-5a5a831ba7c1',
        'Samsung'
    ),
    ('b4b7d51a-3163-4c03-b06d-4b33eb9e2c1d', 'Apple'),
    ('c891e70d-96b6-4d8b-9ebc-9f82c2b7a8d7', 'Google'),
    (
        'd82f0fcd-0c10-4d02-8c59-1f2b4a1d43d8',
        'Microsoft'
    ),
    ('e9d53d78-6a0e-4082-a5d7-5f97a9c3e2f4', 'Nike'),
    ('f4704e7b-5805-4c12-83b6-18887b6f4b5e', 'Adidas'),
    ('gbd97b6e-9a4f-48fc-a7d3-28e9f7e108d1', 'Toyota'),
    ('h63a1c0b-e8a5-4a0a-8455-4e61d82fae1f', 'Honda'),
    ('i24c6f9d-3528-48d5-aa1c-5f8a6b2356e2', 'Sony'),
    (
        'j97f1d9a-2e57-44a4-b4d8-ee8c4e8e5a0d',
        'Coca-Cola'
    );

INSERT INTO
    cliente (id, nombre, apellido, zona)
VALUES
    (
        'cef0cbf364584f13a418ee4d7e7505dd',
        'John',
        'Doe',
        'North'
    ),
    (
        'c0a2b8e22c6549d6b44f7e1901c8a3c1',
        'Jane',
        'Smith',
        'South'
    ),
    (
        'f5e1d8bce5f148dc9e52495db8f1a2b3',
        'Michael',
        'Johnson',
        'East'
    ),
    (
        'a7b4c5d3e2f14e1b2c9d5e6f7a8b9c0d',
        'Emily',
        'Williams',
        'West'
    ),
    (
        'd3e5f2e1f4a6b5c7d8e9f0a1b2c3d4e',
        'David',
        'Brown',
        'Central'
    ),
    (
        'b2e1f4a7b8c9d0e1f2a3b4c5d6e7f8a',
        'Sarah',
        'Jones',
        'Suburb'
    ),
    (
        'e2f1a7b8c9d1e2f3a4b5c6d7e8f9a0b',
        'Jessica',
        'Garcia',
        'Downtown'
    ),
    (
        'a7b8c9d1e2f3a4b5c6d7e8f9a0b1c2d',
        'Matthew',
        'Martinez',
        'Rural'
    ),
    (
        'b8c9d1e2f3a4b5c6d7e8f9a0b1c2d3e',
        'Jennifer',
        'Rodriguez',
        'Urban'
    ),
    (
        'c9d1e2f3a4b5c6d7e8f9a0b1c2d3e4f',
        'Daniel',
        'Hernandez',
        'Metropolitan'
    ),
    (
        'd1e2f3a4b5c6d7e8f9a0b1c2d3e4f5a',
        'Lisa',
        'Lopez',
        'Village'
    ),
    (
        'e2f3a4b5c6d7e8f9a0b1c2d3e4f5a6b',
        'Christopher',
        'Gonzalez',
        'Town'
    ),
    (
        'f3a4b5c6d7e8f9a0b1c2d3e4f5a6b7c',
        'Amanda',
        'Perez',
        'Countryside'
    ),
    (
        'a4b5c6d7e8f9a0b1c2d3e4f5a6b7c8d',
        'James',
        'Wilson',
        'Residential'
    ),
    (
        'b5c6d7e8f9a0b1c2d3e4f5a6b7c8d9e',
        'Megan',
        'Young',
        'Commercial'
    ),
    (
        'c6d7e8f9a0b1c2d3e4f5a6b7c8d9e0f',
        'Ryan',
        'Scott',
        'Industrial'
    ),
    (
        'd7e8f9a0b1c2d3e4f5a6b7c8d9e0f1a',
        'Michelle',
        'King',
        'Historical'
    ),
    (
        'e8f9a0b1c2d3e4f5a6b7c8d9e0f1a2b',
        'Jason',
        'Hill',
        'Modern'
    ),
    (
        'f9a0b1c2d3e4f5a6b7c8d9e0f1a2b3c',
        'Kimberly',
        'Morales',
        'Contemporary'
    ),
    (
        'a0b1c2d3e4f5a6b7c8d9e0f1a2b3c4d',
        'Andrew',
        'Clark',
        'Ancient'
    ),
    (
        'b1c2d3e4f5a6b7c8d9e0f1a2b3c4d5e',
        'Rachel',
        'Lewis',
        'Medieval'
    );

INSERT INTO
    telefono (id, numero, cliente_id)
VALUES
    (
        UUID(),
        '555-123-4567',
        'cef0cbf364584f13a418ee4d7e7505dd'
    ),
    (
        UUID(),
        '555-987-6543',
        'cef0cbf364584f13a418ee4d7e7505dd'
    ),
    (
        UUID(),
        '555-234-5678',
        'c0a2b8e22c6549d6b44f7e1901c8a3c1'
    ),
    (
        UUID(),
        '555-876-5432',
        'c0a2b8e22c6549d6b44f7e1901c8a3c1'
    ),
    (
        UUID(),
        '555-345-6789',
        'f5e1d8bce5f148dc9e52495db8f1a2b3'
    ),
    (
        UUID(),
        '555-765-4321',
        'f5e1d8bce5f148dc9e52495db8f1a2b3'
    ),
    (
        UUID(),
        '555-456-7890',
        'a7b4c5d3e2f14e1b2c9d5e6f7a8b9c0d'
    ),
    (
        UUID(),
        '555-654-3210',
        'a7b4c5d3e2f14e1b2c9d5e6f7a8b9c0d'
    ),
    (
        UUID(),
        '555-567-8901',
        'd3e5f2e1f4a6b5c7d8e9f0a1b2c3d4e'
    ),
    (
        UUID(),
        '555-876-5432',
        'd3e5f2e1f4a6b5c7d8e9f0a1b2c3d4e'
    ),
    (
        UUID(),
        '555-678-9012',
        'b2e1f4a7b8c9d0e1f2a3b4c5d6e7f8a'
    ),
    (
        UUID(),
        '555-987-2109',
        'b2e1f4a7b8c9d0e1f2a3b4c5d6e7f8a'
    ),
    (
        UUID(),
        '555-789-0123',
        'e2f1a7b8c9d1e2f3a4b5c6d7e8f9a0b'
    ),
    (
        UUID(),
        '555-876-5432',
        'e2f1a7b8c9d1e2f3a4b5c6d7e8f9a0b'
    ),
    (
        UUID(),
        '555-890-1234',
        'a7b8c9d1e2f3a4b5c6d7e8f9a0b1c2d'
    ),
    (
        UUID(),
        '555-765-4321',
        'a7b8c9d1e2f3a4b5c6d7e8f9a0b1c2d'
    ),
    (
        UUID(),
        '555-901-2345',
        'b8c9d1e2f3a4b5c6d7e8f9a0b1c2d3e'
    ),
    (
        UUID(),
        '555-654-3210',
        'b8c9d1e2f3a4b5c6d7e8f9a0b1c2d3e'
    ),
    (
        UUID(),
        '555-012-3456',
        'c9d1e2f3a4b5c6d7e8f9a0b1c2d3e4f'
    ),
    (
        UUID(),
        '555-987-6543',
        'c9d1e2f3a4b5c6d7e8f9a0b1c2d3e4f'
    ),
    (
        UUID(),
        '555-234-5678',
        'd1e2f3a4b5c6d7e8f9a0b1c2d3e4f5a'
    ),
    (
        UUID(),
        '555-876-5432',
        'd1e2f3a4b5c6d7e8f9a0b1c2d3e4f5a'
    ),
    (
        UUID(),
        '555-111-2222',
        'e2f3a4b5c6d7e8f9a0b1c2d3e4f5a6b'
    ),
    (
        UUID(),
        '555-333-4444',
        'e2f3a4b5c6d7e8f9a0b1c2d3e4f5a6b'
    ),
    (
        UUID(),
        '555-555-6666',
        'f3a4b5c6d7e8f9a0b1c2d3e4f5a6b7c'
    ),
    (
        UUID(),
        '555-777-8888',
        'f3a4b5c6d7e8f9a0b1c2d3e4f5a6b7c'
    ),
    (
        UUID(),
        '555-999-0000',
        'a4b5c6d7e8f9a0b1c2d3e4f5a6b7c8d'
    ),
    (
        UUID(),
        '555-222-3333',
        'a4b5c6d7e8f9a0b1c2d3e4f5a6b7c8d'
    ),
    (
        UUID(),
        '555-444-5555',
        'b5c6d7e8f9a0b1c2d3e4f5a6b7c8d9e'
    ),
    (
        UUID(),
        '555-666-7777',
        'b5c6d7e8f9a0b1c2d3e4f5a6b7c8d9e'
    ),
    (
        UUID(),
        '555-888-9999',
        'c6d7e8f9a0b1c2d3e4f5a6b7c8d9e0f'
    ),
    (
        UUID(),
        '555-000-1111',
        'c6d7e8f9a0b1c2d3e4f5a6b7c8d9e0f'
    ),
    (
        UUID(),
        '555-123-4567',
        'cef0cbf364584f13a418ee4d7e7505dd'
    ),
    (
        UUID(),
        '555-987-6543',
        'cef0cbf364584f13a418ee4d7e7505dd'
    ),
    (
        UUID(),
        '555-234-5678',
        'c0a2b8e22c6549d6b44f7e1901c8a3c1'
    ),
    (
        UUID(),
        '555-876-5432',
        'c0a2b8e22c6549d6b44f7e1901c8a3c1'
    ),
    (
        UUID(),
        '555-345-6789',
        'f5e1d8bce5f148dc9e52495db8f1a2b3'
    ),
    (
        UUID(),
        '555-765-4321',
        'f5e1d8bce5f148dc9e52495db8f1a2b3'
    ),
    (
        UUID(),
        '555-456-7890',
        'a7b4c5d3e2f14e1b2c9d5e6f7a8b9c0d'
    ),
    (
        UUID(),
        '555-654-3210',
        'a7b4c5d3e2f14e1b2c9d5e6f7a8b9c0d'
    ),
    (
        UUID(),
        '555-567-8901',
        'd3e5f2e1f4a6b5c7d8e9f0a1b2c3d4e'
    ),
    (
        UUID(),
        '555-876-5432',
        'd3e5f2e1f4a6b5c7d8e9f0a1b2c3d4e'
    ),
    (
        UUID(),
        '555-678-9012',
        'b2e1f4a7b8c9d0e1f2a3b4c5d6e7f8a'
    ),
    (
        UUID(),
        '555-987-2109',
        'b2e1f4a7b8c9d0e1f2a3b4c5d6e7f8a'
    ),
    (
        UUID(),
        '555-789-0123',
        'e2f1a7b8c9d1e2f3a4b5c6d7e8f9a0b'
    ),
    (
        UUID(),
        '555-876-5432',
        'e2f1a7b8c9d1e2f3a4b5c6d7e8f9a0b'
    ),
    (
        UUID(),
        '555-890-1234',
        'a7b8c9d1e2f3a4b5c6d7e8f9a0b1c2d'
    ),
    (
        UUID(),
        '555-765-4321',
        'a7b8c9d1e2f3a4b5c6d7e8f9a0b1c2d'
    ),
    (
        UUID(),
        '555-901-2345',
        'b8c9d1e2f3a4b5c6d7e8f9a0b1c2d3e'
    ),
    (
        UUID(),
        '555-111-2222',
        'd7e8f9a0b1c2d3e4f5a6b7c8d9e0f1a'
    ),
    (
        UUID(),
        '555-333-4444',
        'd7e8f9a0b1c2d3e4f5a6b7c8d9e0f1a'
    ),
    (
        UUID(),
        '555-555-6666',
        'e8f9a0b1c2d3e4f5a6b7c8d9e0f1a2b'
    ),
    (
        UUID(),
        '555-777-8888',
        'e8f9a0b1c2d3e4f5a6b7c8d9e0f1a2b'
    ),
    (
        UUID(),
        '555-999-0000',
        'f9a0b1c2d3e4f5a6b7c8d9e0f1a2b3c'
    ),
    (
        UUID(),
        '555-222-3333',
        'f9a0b1c2d3e4f5a6b7c8d9e0f1a2b3c'
    ),
    (
        UUID(),
        '555-111-9999',
        'a0b1c2d3e4f5a6b7c8d9e0f1a2b3c4d'
    ),
    (
        UUID(),
        '555-222-8888',
        'b1c2d3e4f5a6b7c8d9e0f1a2b3c4d5e'
    );

INSERT INTO
    cliente_preferencias (cliente_id, preferencia_id)
SELECT
    cliente_id,
    preferencia_id
FROM
    (
        SELECT
            c.id AS cliente_id,
            p.id AS preferencia_id,
            ROW_NUMBER() OVER (
                PARTITION BY c.id
                ORDER BY
                    RAND()
            ) AS rn
        FROM
            cliente c
            CROSS JOIN preferencia p
    ) AS sub
WHERE
    rn <= 3;

INSERT INTO
    caja (id, identificador)
VALUES
    (
        '4b5c6d7e-8f9a-0b1c-2d3e-4f5a6b7c8d9e',
        'CAJA-001'
    ),
    (
        '6d7e8f9a-0b1c-2d3e-4f5a-6b7c8d9e0f1a',
        'CAJA-002'
    ),
    (
        '8f9a0b1c-2d3e-4f5a-6b7c-8d9e0f1a2b3c',
        'CAJA-003'
    ),
    (
        '1c2d3e4f-5a6b-7c8d-9e0f-1a2b3c4d5e6f',
        'CAJA-004'
    );

INSERT INTO
    turno (id, hora_inicio, hora_fin)
VALUES
    (
        UUID(),
        '08:00:00',
        '12:00:00'
    ),
    (
        UUID(),
        '12:00:00',
        '16:00:00'
    ),
    (
        UUID(),
        '16:00:00',
        '20:00:00'
    );

INSERT INTO
    cargo (id, nombre)
VALUES
    (UUID(), 'cajero'),
    (
        UUID(),
        'reponedor de estanterías'
    ),
    (
        UUID(),
        'encargado de seguridad'
    ),
    (
        UUID(),
        'gerente de tienda'
    );

SELECT
    id INTO @cajero_id
FROM
    cargo
WHERE
    nombre = 'cajero';

INSERT INTO
    empleado (
        id,
        nombre,
        apellido,
        direccion,
        telefono,
        cargo_id
    )
VALUES
    (
        UUID(),
        'María',
        'Rodríguez',
        'Calle Bolívar 1415',
        '777111222',
        @cajero_id
    ),
    (
        UUID(),
        'Juan',
        'García',
        'Av. Las Americas 1617',
        '777333444',
        @cajero_id
    ),
    (
        UUID(),
        'Carla',
        'López',
        'Calle Sucre 1819',
        '777555666',
        @cajero_id
    ),
    (
        UUID(),
        'Roberto',
        'Hernández',
        'Av. San José 2021',
        '777777888',
        @cajero_id
    ),
    (
        UUID(),
        'Laura',
        'Pérez',
        'Calle Potosí 2223',
        '777999000',
        @cajero_id
    ),
    (
        UUID(),
        'Miguel',
        'Martínez',
        'Av. Santa Cruz 2425',
        '777000111',
        @cajero_id
    );

SELECT
    id INTO @emp1_id
FROM
    empleado
WHERE
    nombre = 'María'
    AND apellido = 'Rodríguez';

SELECT
    id INTO @emp2_id
FROM
    empleado
WHERE
    nombre = 'Juan'
    AND apellido = 'García';

SELECT
    id INTO @emp3_id
FROM
    empleado
WHERE
    nombre = 'Carla'
    AND apellido = 'López';

SELECT
    id INTO @emp4_id
FROM
    empleado
WHERE
    nombre = 'Roberto'
    AND apellido = 'Hernández';

SELECT
    id INTO @emp5_id
FROM
    empleado
WHERE
    nombre = 'Laura'
    AND apellido = 'Pérez';

SELECT
    id INTO @emp6_id
FROM
    empleado
WHERE
    nombre = 'Miguel'
    AND apellido = 'Martínez';

INSERT INTO
    empleado_salario (id, sueldo_base, empleado_id)
VALUES
    (UUID(), 1800.00, @emp1_id),
    (UUID(), 2000.00, @emp2_id),
    (UUID(), 2200.00, @emp3_id),
    (UUID(), 1900.00, @emp4_id),
    (UUID(), 2500.00, @emp5_id),
    (UUID(), 2100.00, @emp6_id);

INSERT INTO
    historial_pago (
        id,
        fecha_emision,
        fecha_pago,
        mes_pago,
        empleado_id
    )
VALUES
    (
        UUID(),
        '2023-01-01',
        '2023-01-05',
        'Enero',
        @emp1_id
    ),
    (
        UUID(),
        '2023-02-01',
        '2023-02-05',
        'Febrero',
        @emp1_id
    ),
    (
        UUID(),
        '2023-03-01',
        '2023-03-05',
        'Marzo',
        @emp1_id
    ),
    (
        UUID(),
        '2023-01-01',
        '2023-01-05',
        'Enero',
        @emp2_id
    ),
    (
        UUID(),
        '2023-02-01',
        '2023-02-05',
        'Febrero',
        @emp2_id
    ),
    (
        UUID(),
        '2023-03-01',
        '2023-03-05',
        'Marzo',
        @emp2_id
    ),
    (
        UUID(),
        '2023-01-01',
        '2023-01-05',
        'Enero',
        @emp3_id
    ),
    (
        UUID(),
        '2023-02-01',
        '2023-02-05',
        'Febrero',
        @emp3_id
    ),
    (
        UUID(),
        '2023-03-01',
        '2023-03-05',
        'Marzo',
        @emp3_id
    ),
    (
        UUID(),
        '2023-01-01',
        '2023-01-05',
        'Enero',
        @emp4_id
    ),
    (
        UUID(),
        '2023-02-01',
        '2023-02-05',
        'Febrero',
        @emp4_id
    ),
    (
        UUID(),
        '2023-03-01',
        '2023-03-05',
        'Marzo',
        @emp4_id
    ),
    (
        UUID(),
        '2023-01-01',
        '2023-01-05',
        'Enero',
        @emp5_id
    ),
    (
        UUID(),
        '2023-02-01',
        '2023-02-05',
        'Febrero',
        @emp5_id
    ),
    (
        UUID(),
        '2023-03-01',
        '2023-03-05',
        'Marzo',
        @emp5_id
    ),
    (
        UUID(),
        '2023-01-01',
        '2023-01-05',
        'Enero',
        @emp6_id
    ),
    (
        UUID(),
        '2023-02-01',
        '2023-02-05',
        'Febrero',
        @emp6_id
    ),
    (
        UUID(),
        '2023-03-01',
        '2023-03-05',
        'Marzo',
        @emp6_id
    );

INSERT INTO
    bono (id, monto, razon, historial_pago_id)
VALUES
    (
        UUID(),
        100.00,
        'Desempeño',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp1_id
                AND mes_pago = 'Enero'
        )
    ),
    (
        UUID(),
        150.00,
        'Venta extraordinaria',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp2_id
                AND mes_pago = 'Enero'
        )
    ),
    (
        UUID(),
        75.00,
        'Horas extras',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp3_id
                AND mes_pago = 'Febrero'
        )
    ),
    (
        UUID(),
        50.00,
        'Desempeño',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp4_id
                AND mes_pago = 'Enero'
        )
    ),
    (
        UUID(),
        80.00,
        'Venta extraordinaria',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp5_id
                AND mes_pago = 'Marzo'
        )
    ),
    (
        UUID(),
        60.00,
        'Desempeño',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp6_id
                AND mes_pago = 'Enero'
        )
    );

INSERT INTO
    descuento (id, monto, razon, historial_pago_id)
VALUES
    (
        UUID(),
        50.00,
        'Llegadas tarde',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp1_id
                AND mes_pago = 'Febrero'
        )
    ),
    (
        UUID(),
        20.00,
        'Faltas',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp2_id
                AND mes_pago = 'Marzo'
        )
    ),
    (
        UUID(),
        30.00,
        'Daños materiales',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp3_id
                AND mes_pago = 'Marzo'
        )
    ),
    (
        UUID(),
        25.00,
        'Llegadas tarde',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp4_id
                AND mes_pago = 'Febrero'
        )
    ),
    (
        UUID(),
        40.00,
        'Faltas',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp5_id
                AND mes_pago = 'Febrero'
        )
    ),
    (
        UUID(),
        35.00,
        'Daños materiales',
        (
            SELECT
                id
            FROM
                historial_pago
            WHERE
                empleado_id = @emp6_id
                AND mes_pago = 'Marzo'
        )
    );

INSERT INTO
    historial (id, fecha)
VALUES
    (UUID(), '2024-02-03'),
    (UUID(), '2024-05-06'),
    (UUID(), '2024-02-10'),
    (UUID(), '2024-05-03'),
    (UUID(), '2024-02-06'),
    (UUID(), '2024-05-10'),
    (UUID(), '2024-02-13'),
    (UUID(), '2024-05-17'),
    (UUID(), '2024-02-20'),
    (UUID(), '2024-05-24'),
    (UUID(), '2024-02-27'),
    (UUID(), '2024-05-31'),
    (UUID(), '2024-06-03'),
    (UUID(), '2024-06-07'),
    (UUID(), '2024-06-10'),
    (UUID(), '2024-02-14'),
    (UUID(), '2024-06-17'),
    (UUID(), '2024-06-21'),
    (UUID(), '2024-02-24'),
    (UUID(), '2024-06-28');

INSERT INTO
    historial_turnos (historial_id, turno_id, empleado_id)
SELECT
    h.id AS historial_id,
    t.id AS turno_id,
    e.empleado_id AS empleado_id
FROM
    historial h
    CROSS JOIN turno t
    JOIN (
        SELECT
            h.id AS historial_id,
            t.id AS turno_id,
            e.id AS empleado_id,
            ROW_NUMBER() OVER (
                PARTITION BY h.id,
                t.id
                ORDER BY
                    RAND()
            ) AS rownum
        FROM
            historial h
            CROSS JOIN turno t
            CROSS JOIN empleado e
    ) AS e ON h.id = e.historial_id
    AND t.id = e.turno_id
WHERE
    e.rownum = 1;

INSERT INTO
    historial_cajas (historial_id, caja_id)
SELECT
    h.id AS historial_id,
    c.id AS caja_id
FROM
    historial h
    CROSS JOIN caja c;

INSERT INTO
    producto (
        id,
        codigo,
        nombre,
        peso,
        categoria_id,
        lote_id,
        marca_id
    )
SELECT
    UUID(),
    CONCAT('COD', LPAD(FLOOR(1000 + RAND() * 9000), 4, '0')),
    CONCAT(
        'Producto ',
        ROW_NUMBER() OVER (
            PARTITION BY c.id
            ORDER BY
                RAND()
        ),
        ' Categoría ',
        c.nombre
    ),
    ROUND(RAND() * 10 + 1, 2),
    c.id,
    (
        SELECT
            id
        FROM
            lote
        ORDER BY
            RAND()
        LIMIT
            1
    ), (
        SELECT
            id
        FROM
            marca
        ORDER BY
            RAND()
        LIMIT
            1
    )
FROM
    categoria c
    CROSS JOIN (
        SELECT
            1 AS n
        UNION
        ALL
        SELECT
            2
        UNION
        ALL
        SELECT
            3
    ) AS nums
ORDER BY
    c.id;

INSERT INTO
    producto_colores (producto_id, color_id)
SELECT
    DISTINCT p.id,
    c.id
FROM
    producto p
    CROSS JOIN (
        SELECT
            id
        FROM
            color
        ORDER BY
            RAND()
        LIMIT
            3
    ) AS c
ORDER BY
    p.id;

INSERT INTO
    venta (id, fecha, hora, caja_id, cliente_id)
SELECT
    UUID(),
    h.fecha,
    t.hora_inicio + INTERVAL FLOOR(
        RAND() * (
            TIME_TO_SEC(t.hora_fin) - TIME_TO_SEC(t.hora_inicio)
        )
    ) SECOND,
    (
        SELECT
            id
        FROM
            caja
        ORDER BY
            RAND()
        LIMIT
            1
    ), (
        SELECT
            id
        FROM
            cliente
        ORDER BY
            RAND()
        LIMIT
            1
    )
FROM
    (
        SELECT
            fecha
        FROM
            historial
        ORDER BY
            RAND()
        LIMIT
            20
    ) h
    CROSS JOIN turno t
LIMIT
    20;

INSERT
    IGNORE INTO venta_productos (venta_id, producto_id, cantidad)
SELECT
    v.id AS venta_id,
    (
        SELECT
            id
        FROM
            producto
        WHERE
            id NOT IN (
                SELECT
                    producto_id
                FROM
                    venta_productos
                WHERE
                    venta_id = v.id
            )
        ORDER BY
            RAND()
        LIMIT
            1
    ) AS producto_id,
    FLOOR(1 + RAND() * 10) AS cantidad
FROM
    (
        SELECT
            id,
            fecha
        FROM
            venta
        ORDER BY
            fecha DESC
        LIMIT
            20
    ) v
ORDER BY
    v.fecha DESC;

INSERT
    IGNORE INTO venta_productos (venta_id, producto_id, cantidad)
SELECT
    v.id AS venta_id,
    (
        SELECT
            id
        FROM
            producto
        WHERE
            id NOT IN (
                SELECT
                    producto_id
                FROM
                    venta_productos
                WHERE
                    venta_id = v.id
            )
        ORDER BY
            RAND()
        LIMIT
            1
    ) AS producto_id,
    FLOOR(1 + RAND() * 10) AS cantidad
FROM
    (
        SELECT
            id,
            fecha
        FROM
            venta
        ORDER BY
            fecha DESC
        LIMIT
            20
    ) v
ORDER BY
    v.fecha DESC;

INSERT INTO
    producto_precio (id, precio_compra, precio_venta, producto_id)
SELECT
    UUID(),
    ROUND(RAND() * (200 - 60) + 60, 2) AS precio_compra,
    ROUND(RAND() * (200 - 60) + 60, 2) AS precio_venta,
    p.id
FROM
    producto p;

INSERT INTO
    promocion (id, nombre, precio)
SELECT
    UUID() AS id,
    CONCAT('Promoción ', num) AS nombre,
    ROUND(700 + RAND() * (1000 - 700), 2) AS precio
FROM
    (
        SELECT
            1 AS num
        UNION
        ALL
        SELECT
            2
        UNION
        ALL
        SELECT
            3
        UNION
        ALL
        SELECT
            4
        UNION
        ALL
        SELECT
            5
        UNION
        ALL
        SELECT
            6
        UNION
        ALL
        SELECT
            7
        UNION
        ALL
        SELECT
            8
        UNION
        ALL
        SELECT
            9
        UNION
        ALL
        SELECT
            10
    ) AS numbers;

INSERT INTO
    promocion_calificacion (id, calificacion, promocion_id)
SELECT
    UUID() AS id,
    FLOOR(4 + RAND() * (6 - 4 + 1)) AS calificacion,
    p.id AS promocion_id
FROM
    promocion p;

INSERT INTO
    promocion_duracion (id, fecha_inicio, fecha_fin, promocion_id)
SELECT
    UUID() AS id,
    DATE_FORMAT(
        CONCAT('2024-', LPAD(1 + RAND() * 2, 2, '0'), '-01'),
        '%Y-%m-%d'
    ) AS fecha_inicio,
    DATE_FORMAT(
        DATE_ADD(
            DATE_FORMAT(
                CONCAT('2024-', LPAD(1 + RAND() * 2, 2, '0'), '-01'),
                '%Y-%m-%d'
            ),
            INTERVAL 2 MONTH
        ),
        '%Y-%m-%d'
    ) AS fecha_fin,
    p.id AS promocion_id
FROM
    promocion p
ORDER BY
    RAND();

INSERT
    IGNORE INTO promocion_productos (promocion_id, producto_id)
SELECT
    p.id AS promocion_id,
    (
        SELECT
            id
        FROM
            producto
        ORDER BY
            RAND()
        LIMIT
            1
    ) AS producto_id
FROM
    promocion p;

INSERT
    IGNORE INTO promocion_productos (promocion_id, producto_id)
SELECT
    p.id AS promocion_id,
    (
        SELECT
            id
        FROM
            producto
        ORDER BY
            RAND()
        LIMIT
            1
    ) AS producto_id
FROM
    promocion p;

INSERT
    IGNORE INTO promocion_productos (promocion_id, producto_id)
SELECT
    p.id AS promocion_id,
    (
        SELECT
            id
        FROM
            producto
        ORDER BY
            RAND()
        LIMIT
            1
    ) AS producto_id
FROM
    promocion p;

INSERT INTO
    venta_promociones (venta_id, promocion_id, cantidad)
SELECT
    v.id AS venta_id,
    p.id AS promocion_id,
    1 AS cantidad
FROM
    venta v
    CROSS JOIN (
        SELECT
            id
        FROM
            promocion
        ORDER BY
            RAND()
        LIMIT
            1
    ) p;

INSERT INTO
    venta_total (id, total, venta_id)
SELECT
    UUID() AS id,
    SUM(vp.cantidad * pp.precio_venta) + SUM(vpr.cantidad * p.precio) AS total,
    v.id AS venta_id
FROM
    venta v
    JOIN venta_productos vp ON v.id = vp.venta_id
    JOIN producto_precio pp ON vp.producto_id = pp.producto_id
    JOIN venta_promociones vpr ON v.id = vpr.venta_id
    JOIN promocion p ON vpr.promocion_id = p.id
GROUP BY
    v.id;
